public class Skip extends Colourful implements Special {
    
    public Skip(char colour) {
        super(colour);
    }

    public void specialEffect() {
        System.out.println("next player skips turn");
    }
}
