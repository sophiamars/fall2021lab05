public class PickUp2 extends Colourful implements Special {
    
    public PickUp2(char colour) {
        super(colour);
    }

    public void specialEffect() {
        System.out.println("next player picks up 2 cards");
    }
}
