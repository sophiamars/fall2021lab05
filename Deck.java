import java.util.ArrayList;

public class Deck {
    private ArrayList<Card> deck;

    public Deck() {
        this.deck = new ArrayList<Card>();
        char[] colours = new char[]{'r', 'g', 'b', 'y'};

        for (int i = 0; i < colours.length; i++) {
            this.deck.add(new Wild());
            this.deck.add(new WildPickUp4());

            for (int j = 0; j < 2; j++) {
                this.deck.add(new Skip(colours[i]));
                this.deck.add(new Reverse(colours[i]));
                this.deck.add(new PickUp2(colours[i]));

                for (int k = 0; k < 10; k++) {
                    this.deck.add(new Numbered(colours[i], k));
                }
            }
        }
    }

    public void addToDeck(Card newCard) {
        this.deck.add(newCard);
    }

    public Card draw() {
        Card card = this.deck.get(0);
        this.deck.remove(0);
        return card;
    }

    public ArrayList<Card> getDeck() {
        return this.deck;
    }
}
