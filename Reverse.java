public class Reverse extends Colourful implements Special {
    
    public Reverse(char colour) {
        super(colour);
    }

    public void specialEffect() {
        System.out.println("play swaps direction");
    }
}
