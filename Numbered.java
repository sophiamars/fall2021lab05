public class Numbered extends Colourful {
    private int number;

    public Numbered(char colour, int number) {
        super(colour);
        this.number = number;
    }

    public int getNumber() {
        return this.number;
    }
}
