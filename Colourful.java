public abstract class Colourful implements Card {
    protected char colour;
    
    public Colourful(char colour) {
        this.colour = colour;
    }

    public char getColour() {
        return this.colour;
    }

    public boolean canPlay(Card prevCard, Card currCard) {
        boolean valid;

        if (prevCard instanceof Wild) {valid = true;}
        else if (((Colourful)prevCard).getColour() == this.colour) {valid =  true;}
        else if (prevCard.getClass() == currCard.getClass()) {
            if (prevCard instanceof Numbered) {
                if (((Numbered)prevCard).getNumber() == ((Numbered)currCard).getNumber()) {
                    valid = true;
                }
                else {valid = false;}
            }
            else {valid = true;}
        }
        else {valid = false;}

        return valid;
    }
}
