//Sophia Marshment - 2038832

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class CardTest {

    // TEST DECK

    @Test
    public void testDrawCard() {
        Deck deck = new Deck();

        assertTrue(deck.draw() instanceof Wild);
        assertTrue(deck.draw() instanceof WildPickUp4);
        assertTrue(deck.draw() instanceof Skip);
        assertTrue(deck.draw() instanceof Reverse);
        assertTrue(deck.draw() instanceof PickUp2);
        assertTrue(deck.draw() instanceof Numbered);
    }

    @Test
    public void testAddToDeck() {
        Deck deck = new Deck();

        deck.addToDeck(new Reverse('g'));

        assertTrue(deck.getDeck().get(deck.getDeck().size()-1) instanceof Reverse);
    }

    // TEST INDIVIDUAL CARDS

    @Test
    public void canPlayPrevWild() {
        Card card = new Skip('y');

        assertTrue(card.canPlay(new Wild(), card));
        assertTrue(card.canPlay(new WildPickUp4(), card));
    }

    @Test
    public void canPlayCurrWild() {
        Card card = new Wild();

        assertTrue(card.canPlay(new Skip('g'), card));
        assertTrue(card.canPlay(new Reverse('b'), card));
    }

    @Test
    public void canPlayPrevCurrWild() {
        Card card = new Wild();

        assertTrue(card.canPlay(new Wild(), card));
        assertTrue(card.canPlay(new WildPickUp4(), card));
    }

    @Test
    public void canPlayTestColour() {
        Card card = new Numbered('r', 4);

        assertTrue(card.canPlay(new Reverse('r'), card));
        assertFalse(card.canPlay(new Skip('b'), card));
    }
    
    @Test
    public void canPlayTestNumber() {
        Card card = new Numbered('r', 2);

        assertTrue(card.canPlay(new Numbered('b', 2), card));
        assertFalse(card.canPlay(new Numbered('b', 6), card));
    }
    
    @Test
    public void canPlayTestSpecialType() {
        Card card = new Reverse('r');

        assertTrue(card.canPlay(new Reverse('b'), card));
        assertFalse(card.canPlay(new Skip('y'), card));
    }
}
