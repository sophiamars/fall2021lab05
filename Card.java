public interface Card {
    boolean canPlay(Card prevCard, Card currCard);
}